#!/usr/bin/python
# -*- coding: utf-8 -*-
"""module setup."""

import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='pumpwood_models',
    version='0.30',
    packages=find_packages(),
    include_package_data=True,
    license='',  # example license
    description='Package for creation of mathematical model on Pumpwood',
    long_description=README,
    url='',
    author='André Andrade Baceti',
    author_email='a.baceti@murabei.com',
    classifiers=[
    ],
    install_requires=['requests',
                      'simplejson',
                      'pandas',
                      "pumpwood-communication>=0.25",
                      "pumpwood-miscellaneous>=0.18",
                      "pyarrow==3.0.0"],
    dependency_links=[]
)
