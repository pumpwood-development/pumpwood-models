# -*- coding: utf-8 -*-
# import grequests está dando erro no https quando inclui esse pacote
import requests
import simplejson as json
import math
import copy

from .exceptions import exceptions_dict\
                      , ObjectDoesNotExist\
                      , PumpWoodUnauthorized\
                      , ModelWronglyDefined\
                      , WrongModelEstimationWorkflow\
                      , WrongDataLoading\
                      , PumpWoodException\
                      , PumpWoodForbidden

from .serializers import PumpWoodJSONEncoder

def batch(iterable, n=1):
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]

class PumpWoodMicroService():
  '''
    :param name: Name of the microservice, helps when exceptions 
      are raised
    :type name: str
    :param server_url: url of the server that will be connected
    :type server_url: str
    :param user_name: Username that will be logged on.
    :type user_name: str
    :param password: Variable to be converted to JSON and posted along with 
      the request
    :type password: str
    :param local: Boolean if the service is to be considered local so have no 
      conections with other PumpWoods
    :type local: bool

    Class to define an inter-pumpwood MicroService
  '''
  _base_header = {'Content-Type': 'application/json'}
  _auth_header = None

  def __init__(self, name:str, server_url:str=None, username:str=None
    , password:str=None, verify_ssl:bool=True):

    self.name = name
    self._headers = None
    self._username = username
    self._password = password
    self.server_url = server_url
    self.verify_ssl = verify_ssl
  #
  @staticmethod
  def angular_json(request_result):
    string_start = ")]}',\n"
    try:
      if request_result.text[:6] == string_start:
        return ( json.loads(request_result.text[6:]) )
      else:
        return ( json.loads(request_result.text) )
    except:
      return {"error": "Can not decode to Json", 'msg': request_result.text}
  #
  def login(self):
    login_url = self.server_url + '/rest/registration/login/'

    login_result = requests.post(login_url
      , data=json.dumps({'username': self._username, 'password': self._password})
      , headers=self._base_header
      , verify=self.verify_ssl
    )
    
    login_data = PumpWoodMicroService.angular_json(login_result)
    if login_result.status_code != 200:
      raise Exception( json.dumps(login_data) )

    self._auth_header ={ 'Authorization': login_data['token'] }
  #
  def _check_auth_header(self, auth_header):
    if self._auth_header is None:
      if auth_header is None:
        raise PumpWoodUnauthorized('MicroService {name} not looged and auth_header not provided'.format(
          name=self.name
        ))
      else:
        auth_header.update(self._base_header)
        return auth_header
    else:
      if auth_header is not None:
        raise PumpWoodUnauthorized('MicroService {name} already looged and auth_header was provided'.format(
          name=self.name
        ))
      else:
        temp_auth_header = self._auth_header.copy()
        temp_auth_header.update(self._base_header)
        return temp_auth_header
  #
  def error_handler(cls, response):
    if ( math.floor(response.status_code / 100) ) != 2:
      response_dict = PumpWoodMicroService.angular_json(response)
      exception_type = exceptions_dict.get('type')
      if exception_type is not None:
        raise exceptions_dict[response_dict['type']]( response_dict['msg'] )
      else:
        raise Exception( str(response_dict) )
  #
  def post(self, url, data, auth_header:dict=None):
    request_header = self._check_auth_header(auth_header=auth_header)
    
    post_url  = self.server_url + url
    response = requests.post(url=post_url, data=json.dumps(data, cls=PumpWoodJSONEncoder
      , ignore_nan=True), verify=self.verify_ssl, headers=request_header)
    self.error_handler(response)

    return PumpWoodMicroService.angular_json(response)

  def get(self, url, auth_header:dict=None):
    request_header = self._check_auth_header(auth_header)
    
    post_url = self.server_url + url
    response = requests.get(post_url, verify=self.verify_ssl
      , headers=request_header)
    self.error_handler(response)

    return PumpWoodMicroService.angular_json(response)

  def request_delete(self, url, auth_header:dict=None):
    request_header = self._check_auth_header(auth_header)
    
    post_url = self.server_url + url
    response = requests.delete(post_url, verify=self.verify_ssl
      , headers=request_header)
    self.error_handler(response)

    return PumpWoodMicroService.angular_json(response)

  def list(self, model_class:str, filter_dict:dict={}, exclude_dict:dict={}
    , order_by:list=[], auth_header:dict=None)->list:
    '''Function to post at list end-point (resumed data) of PumpWood like systems, results will be 
    paginated. To get next pag, send recived pk at exclude dict (ex.: exclude_dict={id__in: [1,2,...,30]}).
       
    Args:
      model_class (str): Model class of the end-point
    
    Kwargs:
      filter_dict (dict): Filter dict to be used at the query
        (objects.filter arguments).
      exclude_dict (dict):  Exclude dict to be used at the query
        (objects.exclude arguments).
      order_by (list): Ordering list to be used at the query
        (objects.order_by arguments).
      auth_header(dict): Dictionary containing the auth header.

    Returns:
      list: Contaiing objects serialized by list Serializer.

    Raises:
      No especific raises.

    Example:
      No example yet.
    '''
    url_str = "/rest/%s/list/" % ( model_class.lower(), )
    post_data = {'filter_dict': filter_dict, 'exclude_dict': exclude_dict
      , 'order_by': order_by}
    return self.post(url=url_str, data=post_data, auth_header=auth_header)

  def list_without_pag(self, model_class:str, filter_dict:dict={}
    , exclude_dict:dict={}, order_by:list=[], auth_header:dict=None)->list:
    '''Function to post at list end-point (resumed data) of PumpWood like systems, results won't be paginated.
    **Be carefull with large returns.**
       
    Args:
      model_class (str): Model class of the end-point
    
    Kwargs:
      filter_dict (dict): Filter dict to be used at the query 
        (objects.filter arguments)
      exclude_dict (dict):  Exclude dict to be used at the query
        (objects.exclude arguments)
      order_by (list): Ordering list to be used at the query
        (objects.order_by arguments)
      auth_header(dict): Dictionary containing the auth header.

    Returns:
      list: Contaiing objects serialized by list Serializer.

    Raises:
      No especific raises.

    Example:
      No example yet.
    '''
    url_str = "/rest/%s/list-without-pag/" % ( model_class.lower(), )
    post_data = {'filter_dict': filter_dict, 'exclude_dict': exclude_dict, 'order_by': order_by}
    return self.post(url=url_str, data=post_data)

  def retrieve(self, model_class:str, pk:int, auth_header:dict=None)->dict:
    '''Function to get object serialized by retrieve end-point (more detailed data)
       
    Args:
      model_class (str): Model class of the end-point
      pk (int): Object pk
    
    Kwargs:
      auth_header(dict): Dictionary containing the auth header.

    Returns:
      list: Contaiing objects serialized by retrieve Serializer.

    Raises:
      No especific raises.

    Example:
      No example yet.
    '''
    url_str = "/rest/%s/retrieve/%d/" % ( model_class.lower(), pk)
    return self.get( url=url_str, auth_header=auth_header )

  def save(self, obj_dict, auth_header:dict=None):
    '''Function to save or update a new model_class object. If obj_dict{'pk'} is None or
    not defined a new object will be created. The obj model class is defided at obj_dict['model_class']
    and if not defined an PumpWoodException will be raised.
       
    Args:
      obj_dict (dict): Model data dictionary. It must have 'model_class' key and if 'pk' key
      is not defined a new object will be created, else object with pk will be updated.
    
    Kwargs:
      auth_header(dict): Dictionary containing the auth header.

    Returns:
      dict: Updated/Created object data.

    Raises:
      PumpWoodException('To save an object obj_dict must have model_class defined.'): Will be raised if
      model_class key is not present on obj_dict dictionary

    Example:
      No example yet.
    '''
    model_class = obj_dict.get('model_class')
    if model_class is None:
      raise PumpWoodException('To save an object obj_dict must have model_class defined.')
    
    url_str = "/rest/%s/save/" % ( model_class.lower())
    return self.post( url=url_str, data=obj_dict )

  def delete(self, model_class:str, pk:int, auth_header:dict=None):
    '''Delete (or whatever the PumpWood system have been implemented) the object with the specified pk.
       
      Args:
        model_class (str): Model class to delete the object
        pk (int): Object pk to be deleted (or whatever the PumpWood system have been implemented)
      
      Kwargs:
        auth_header(dict): Dictionary containing the auth header.

      Returns:
        Dependends on backend implementation

      Raises:
        Dependends on backend implementation

      Example:
        No example yet.
    '''
    url_str = "/rest/%s/delete/%d/" % ( model_class.lower(), pk )
    return self.request_delete( url=url_str )
    

  def list_actions(self, model_class:str, auth_header:dict=None):
    '''Return a list of all actions avaiable at this model class.
       
    Args:
      model_class (str): Model class to list possible actions.
    
    Kwargs:
      auth_header(dict): Dictionary containing the auth header.

    Returns:
      list: List of possible actions and its descriptions

    Raises:
      Dependends on backend implementation

    Example:
      No example yet.
    '''
    url_str = "/rest/%s/actions/" % ( model_class.lower())
    return self.get( url=url_str )

  def execute_action(self, model_class:str, action:str, pk:int=None
    , parameters={}, auth_header:dict=None):
    '''Execute action exposed. If action is static or classfunction no pk is necessary.
       
    Args:
      model_class (str): Model class to delete the object
      action (str): Action that will be performed.
    
    Kwargs:
      auth_header(dict): Dictionary containing the auth header.

    Returns:
      pk (int): Pk of the object that action will be performed over.
      parameters (dict): Parameter dictionary to use in the action.

    Raises:
      dict: Return a dict with four keys:
        - result: Result of the action.
        - action: Action description.
        - parameters: Parameters used to perform action.
        - obj: Object over which were performed the action.

    Example:
      No example yet.
    '''
    url_str = "/rest/%s/actions/%s/" % ( model_class.lower(), action)
    if pk is not None:
      url_str = url_str + str(pk) + '/'
    return self.post( url=url_str, data=parameters )

  def search_options(self, model_class:str, auth_header:dict=None):
    '''Returns options to search, like forenging keys and choice fields.
       
    Args:
      model_class (str): Model class to check search parameters
    
    Kwargs:
      auth_header(dict): Dictionary containing the auth header.

    Returns:
      dict: Dictionary with search parameters

    Raises:
      Dependends on backend implementation

    Example:
      No example yet.
    '''
    url_str = "/rest/%s/options/" % ( model_class.lower() )
    return self.get( url=url_str )

  def fill_options(self, model_class, parcial_obj_dict
    , auth_header:dict=None):
    '''Returns options for object fields. This function send partial fillment and return options to finish object fillment.
       
    Args:
      model_class (str): Model class to check filment options.
      parcial_obj_dict (dict): Partial object data
    
    Kwargs:
      auth_header(dict): Dictionary containing the auth header.

    Returns:
      dict: Dictionary with possible data.

    Raises:
      Dependends on backend implementation

    Example:
      No example yet.
    '''
    url_str = "/rest/%s/options/" % ( model_class.lower() )
    return self.post( url=url_str, data=parcial_obj_dict )

  def pivot(self, model_class:str, columns:list=[], format:str='list'
    , filter_dict:dict={}, exclude_dict:dict={}, order_by:list=[]
    , auth_header:dict=None):
    '''Pivots object data acording to columns specified.
       
    Args:
      model_class (str): Model class to be pivoted.
      columns (str): Fields to be used as columns.
      format (str): Format to be used to convert pandas.DataFrame to dictionary.
      filter_dict (dict): Dictionary to to be used in objects.filter argument (Same as list end-point).
      exclude_dict (dict): Dictionary to to be used in objects.exclude argument (Same as list end-point).
      order_by (list): Dictionary to to be used in objects.order_by argument (Same as list end-point).

    Kwargs:
      auth_header(dict): Dictionary containing the auth header.

    Returns:
      dict or list: Depends on format type used to convert pandas.DataFrame

    Raises:
      Dependends on backend implementation

    Example:
      No example yet.
    '''
    url_str = "/rest/%s/pivot/" % ( model_class.lower(), )
    post_data = {'columns':columns, 'format':format, 'filter_dict': filter_dict, 'exclude_dict': exclude_dict, 'order_by': order_by}
    return self.post( url=url_str, data=parcial_obj_dict )

    
  #########################################
  #########################################
  #########Bach assyncronous calls#########
  #########################################
  #########################################
  #########################################
  # def parallel_post(self, url_list:list, data:dict, max_parallel_calls:int
  #   , return_details:bool=False, auth_header:dict=None ):
  #   '''
  #     Create assync calls to parallel_post
  #   '''
  #   request_header = self._check_auth_header(auth_header)

  #   urls = []
  #   body = []
  #   results = []
  #   for batch_index in batch(range(len(data)), max_parallel_calls*4):
  #     requests_list = []
  #     for i in batch_index:
  #       urls.append( url_list[i] )
  #       url_temp = post_url  = self.server_url + url_list[i]
  #       body.append( data[i] )
  #       post_data = json.dumps( data[i] )
  #       requests_list.append( grequests.post(url=url_temp, data=post_data
  #         , verify=self.verify_ssl, headers=request_header) )

  #     responses = grequests.map(requests_list, size=max_parallel_calls)
  #     for r in responses:
  #       results.append( PumpWoodMicroService.angular_json( r ) )
    
  #   if return_details:
  #     to_return = []
  #     for i in range(len(urls)):
  #       to_return.append({'url': urls[i], 'body': body[i], 'results': results[i]})
  #     return to_return

  #   else:
  #     return results

  # def parallel_get(self, url_list:list, max_parallel_calls:int
  #   , return_details:bool=False, auth_header:dict=None):
  #   '''
  #     Create assync calls to parallel_get
  #   '''
  #   request_header = self._check_auth_header(auth_header)

  #   urls = []
  #   results = []
  #   for batch_index in batch(range(len(url_list)), max_parallel_calls*4):
  #     requests_list = []
  #     i = batch_index[0]
  #     for i in batch_index:
  #       urls.append( url_list[i] )
  #       url_temp = post_url  = self.server_url + url_list[i]
  #       requests_list.append( grequests.get(url=url_temp, verify=self.verify_ssl
  #         , session=self.request_session, headers=request_header) )

  #     responses = grequests.map(requests_list, size=max_parallel_calls)
  #     for r in responses:
  #       results.append( PumpWoodMicroService.angular_json( r ) )
    
  #   if return_details:
  #     to_return = []
  #     for i in range(len(urls)):
  #       to_return.append({'url': urls[i], 'results': results[i]})
  #     return to_return
  #   else:
  #     return results

  # def parallel_delete(self, url_list:list, max_parallel_calls:int
  #   , return_details:bool=False, auth_header:dict=None):
  #   '''
  #     Create assync calls to parallel_delete
  #   '''
  #   request_header = self._check_auth_header(auth_header)

  #   urls = []
  #   results = []
  #   for batch_index in batch(range(len(url_list)), max_parallel_calls*4):
  #     requests_list = []
  #     i = batch_index[0]
  #     for i in batch_index:
  #       urls.append( url_list[i] )
  #       url_temp = post_url  = self.server_url + url_list[i]
  #       requests_list.append( grequests.delete(url=url_temp, verify=self.verify_ssl
  #         , session=self.request_session, headers=request_header) )

  #     responses = grequests.map(requests_list, size=max_parallel_calls)
  #     for r in responses:
  #       results.append( PumpWoodMicroService.angular_json( r ) )
    
  #   if return_details:
  #     to_return = []
  #     for i in range(len(urls)):
  #       to_return.append({'url': urls[i], 'results': results[i]})
  #   else:
  #     return results

  # def parallel_list(self, model_class:str, query_list:list=[]
  #   , max_parallel_calls:int=20, return_details:bool=False
  #   , auth_header:dict=None):
  #   '''Function to post at list end-point (resumed data) of PumpWood like 
  #   systems, results will be paginated. To get next pag, send recived pk at
  #    exclude dict (ex.: exclude_dict={id__in: [1,2,...,30]}).
       
  #       Args:
  #         model_class (str): Model class of the end-point
        
  #       Kwargs:
  #         filter_dict (dict): Filter dict to be used at the query (objects.filter arguments)
  #         exclude_dict (dict):  Exclude dict to be used at the query (objects.exclude arguments)
  #         order_by (list): Ordering list to be used at the query (objects.order_by arguments)

  #       Returns:
  #         list: Contaiing objects serialized by list Serializer.

  #       Raises:
  #         No especific raises.

  #       Example:
  #         No example yet.
  #   '''
  #   url_str = "/rest/%s/list/" % ( model_class.lower(), )
  #   data = []
  #   for q in query_list:
  #     if type(q) != dict:
  #       raise PumpWoodException('All members of query_list must be dict')
  #     else:
  #       dict_to_add = {'filter_dict': q.get('filter_dict', {})
  #                    , 'exclude_dict': q.get('exclude_dict', {})
  #                    , 'order_by': q.get('order_by', [])}
  #       data.append( dict_to_add )
  #   return self.parallel_post(url_list=url_str, data=data, max_parallel_calls=max_parallel_calls
  #     , return_details=return_details, auth_header=auth_header)

  # def parallel_list_without_pag(self, model_class:str, query_list:list=[]
  #   , max_parallel_calls:int=20, return_details:bool=False
  #   , auth_header:dict=None):
  #   '''Function to post at list end-point (resumed data) of PumpWood like
  #   systems, results won't be paginated.
  #       **Be carefull with large returns.**
       
  #       Args:
  #         model_class (str): Model class of the end-point
        
  #       Kwargs:
  #         filter_dict (dict): Filter dict to be used at the query (objects.filter arguments)
  #         exclude_dict (dict):  Exclude dict to be used at the query (objects.exclude arguments)
  #         order_by (list): Ordering list to be used at the query (objects.order_by arguments)

  #       Returns:
  #         list: Contaiing objects serialized by list Serializer.

  #       Raises:
  #         No especific raises.

  #       Example:
  #         No example yet.
  #   '''
  #   url_str = "/rest/%s/list-without-pag/" % ( model_class.lower(), )
  #   data = []
  #   for q in query_list:
  #     if type(q) != dict:
  #       raise PumpWoodException('All members of query_list must be dict')
  #     else:
  #       dict_to_add = {'filter_dict': q.get('filter_dict', {})
  #                    , 'exclude_dict': q.get('exclude_dict', {})
  #                    , 'order_by': q.get('order_by', [])}
  #       data.append( dict_to_add )
  #   return self.parallel_post(url_list=url_str, data=data
  #     , max_parallel_calls=max_parallel_calls, return_details=return_details
  #     , auth_header=auth_header)

  # def parallel_retrieve(self, model_class:str, pk_list:list=[]
  #   , max_parallel_calls:int=20, return_details:bool=False, auth_header:dict=None):
  #   '''Function to get object serialized by retrieve end-point (more detailed data)
       
  #       Args:
  #         model_class (str): Model class of the end-point
  #         pk (int): Object pk

  #       Returns:
  #         list: Contaiing objects serialized by retrieve Serializer.

  #       Raises:
  #         No especific raises.

  #       Example:
  #         No example yet.
  #   '''
  #   url_str_list = []
  #   for pk in pk_list:
  #     url_str_list.append( "/rest/%s/retrieve/%d" % ( model_class.lower(), pk) )
  #   return self.parallel_get( url_list=url_str_list, max_parallel_calls=max_parallel_calls
  #     , return_details=return_details, auth_header=auth_header)

  # def parallel_save(self, obj_dict_list:list=[], max_parallel_calls:int=20
  #   , return_details:bool=False, auth_header:dict=None):
  #   '''Function to save or update a new model_class object. If obj_dict{'pk'}
  #   is None or not defined a new object will be created. The obj model class
  #   is defided at obj_dict['model_class']and if not defined an PumpWoodException
  #   will be raised.
       
  #       Args:
  #         obj_dict (dict): Model data dictionary. It must have 'model_class' key and if 'pk' key
  #         is not defined a new object will be created, else object with pk will be updated.

  #       Returns:
  #         dict: Updated/Created object data.

  #       Raises:
  #         PumpWoodException('To save an object obj_dict must have model_class defined.'): Will be raised if
  #         model_class key is not present on obj_dict dictionary

  #       Example:
  #         No example yet.
  #   '''
  #   urls_list = []
  #   for obj_dict in obj_dict_list:
  #     urls_list.append( "/rest/%s/save/" % ( obj_dict['model_class'].lower() ) )
  #   return self.parallel_post( url_list=urls_list, data=obj_dict_list
  #     , max_parallel_calls=max_parallel_calls, return_details=return_details
  #     , auth_header=auth_header)

  # def parallel_delete(self, model_class:str, pk_list:list=[]
  #   , max_parallel_calls:int=20, return_details:bool=False, auth_header:dict=None):
  #   '''Delete (or whatever the PumpWood system have been implemented) the object with the specified pk.
       
  #       Args:
  #         model_class (str): Model class to delete the object
  #         pk (int): Object pk to be deleted (or whatever the PumpWood system have been implemented)

  #       Returns:
  #         Dependends on backend implementation

  #       Raises:
  #         Dependends on backend implementation

  #       Example:
  #         No example yet.
  #   '''
  #   url_list = []
  #   for pk in pk_list:
  #     url_list.append( "/rest/%s/delete/%d" % ( model_class.lower(), pk ) )
  #   return self.delete( url_list=url_list, max_parallel_calls=max_parallel_calls
  #     , return_details=return_details, auth_header=auth_header)


  # def parallel_execute_action(self, model_class:str, action_list:list, pk_list:list
  #   , parameters_list:list, max_parallel_calls:int=20, return_details:bool=False
  #   , auth_header:dict=None):
  #   '''Execute action exposed. If action is static or classfunction no pk is necessary.
       
  #       Args:
  #         model_class (str): Model class to delete the object
  #         action (str): Action that will be performed.

  #       Returns:
  #         pk (int): Pk of the object that action will be performed over.
  #         parameters (dict): Parameter dictionary to use in the action.

  #       Raises:
  #         dict: Return a dict with four keys:
  #           - result: Result of the action.
  #           - action: Action description.
  #           - parameters: Parameters used to perform action.
  #           - obj: Object over which were performed the action.

  #       Example:
  #         No example yet.
  #   '''
  #   if not(len(action_list) == len(pk_list) and len(pk_list) == len(parameters_list)):
  #     raise PumpWoodException('Length of action_list, pk_list and parameters_list must be the same')

  #   url_list = []
  #   for i in range(len(action_list)):
  #     url_str = "/rest/%s/actions/%s/" % ( model_class.lower(), action_list[i])
  #     pk = pk_list[i]
  #     if pk is not None:
  #       url_str = url_str + str(pk) + '/'
  #     url_list.append( url_str )
    
  #   return self.parallel_post( url_list=url_list, data=parameters_list
  #     , max_parallel_calls=max_parallel_calls, return_details=return_details
  #     , auth_header=auth_header )

  # def parallel_pivot(self, model_class, query_list=[], max_parallel_calls=20
  #   , return_details=False, auth_header:dict=None):
  #   '''Pivots object data acording to columns specified.
       
  #       Args:
  #         model_class (str): Model class to be pivoted.
  #         columns (str): Fields to be used as columns.
  #         format (str): Format to be used to convert pandas.DataFrame to dictionary.
  #         filter_dict (dict): Dictionary to to be used in objects.filter argument (Same as list end-point).
  #         exclude_dict (dict): Dictionary to to be used in objects.exclude argument (Same as list end-point).
  #         order_by (list): Dictionary to to be used in objects.order_by argument (Same as list end-point).
        
  #       Returns:
  #         dict or list: Depends on format type used to convert pandas.DataFrame

  #       Raises:
  #         Dependends on backend implementation

  #       Example:
  #         No example yet.
  #   '''

  #   url_str = "/rest/%s/pivot/" % ( model_class.lower(), )
  #   data = []
  #   for q in query_list:
  #     if type(q) != dict:
  #       raise PumpWoodException('All members of query_list must be dict')
  #     else:
  #       dict_to_add = {'filter_dict': q.get('filter_dict', {})
  #                    , 'exclude_dict': q.get('exclude_dict', {})
  #                    , 'order_by': q.get('order_by', [])
  #                    , 'columns': q.get('columns', [])
  #                    , 'format': q.get('format', 'list')}
  #       data.append( dict_to_add )
  #   return self.parallel_post(url_list=url_str, data=data
  #     , max_parallel_calls=max_parallel_calls, return_details=return_details
  #     , auth_header=auth_header)