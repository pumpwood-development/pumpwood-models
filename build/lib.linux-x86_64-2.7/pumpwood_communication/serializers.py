from simplejson import JSONEncoder
from datetime import datetime
from datetime import date
from datetime import time
from pandas import Timestamp

class PumpWoodJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        if isinstance(obj, Timestamp):
            return obj.isoformat()
        if isinstance(obj, date):
            return obj.isoformat()
        if isinstance(obj, time):
            return obj.isoformat()
        return JSONEncoder.default(self, obj)