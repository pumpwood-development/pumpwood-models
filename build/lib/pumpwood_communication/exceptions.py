"""
Define PumpWood exceptions to be treated as API errors.

Define especific errors for PumpWood plataform. These errors will be treated
and will not result in default 500 errors
"""
from .serializers import pumpJsonDump


class PumpWoodException(Exception):
    status_code = 400

    def __repr__(self):
        template = "{class_name}[status_code={status_code}]: " + \
            "{message}\nerror payload={payload}"
        return template.format(
            class_name=self.__class__.__name__,
            status_code=self.status_code, message=self.message,
            payload=self.payload,)

    def __str__(self):
        template = "{class_name}[status_code={status_code}]: " + \
            "{message}\nerror payload={payload}"
        return template.format(
            class_name=self.__class__.__name__,
            status_code=self.status_code, message=self.message,
            payload=self.payload,)

    def __init__(self, message: str, payload: dict = {}, status_code=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = {
            "payload": self.payload,
            "type": self.__class__.__name__,
            "message": self.message}
        return rv


class PumpWoodDataLoadingException(PumpWoodException):
    """Problem when loading data at dataloaders and to_load models."""

    pass


class PumpWoodDataTransformationException(PumpWoodException):
    """Problem when transforming model data."""

    pass


class PumpWoodObjectSavingException(PumpWoodException):
    """Raise for errors in object deserialization."""

    pass


class PumpWoodActionArgsException(PumpWoodException):
    """Missing arguments to perform action."""

    pass


class PumpWoodUnauthorized(PumpWoodException):
    """User Unauthorized to perform action."""

    status_code = 401


class PumpWoodForbidden(PumpWoodException):
    """Action not pemirted independent of authentication."""

    status_code = 403


class PumpWoodObjectDoesNotExist(PumpWoodException):
    """Object not found in database."""

    status_code = 404


"""Exception dictionary to help in error treatment."""
exceptions_dict = {
    "PumpWoodException": PumpWoodException,
    "PumpWoodDataLoadingException": PumpWoodDataLoadingException,
    "PumpWoodObjectSavingException": PumpWoodObjectSavingException,
    "PumpWoodActionArgsException": PumpWoodActionArgsException,
    "PumpWoodUnauthorized": PumpWoodUnauthorized,
    "PumpWoodForbidden": PumpWoodForbidden,
    "PumpWoodObjectDoesNotExist": PumpWoodObjectDoesNotExist}


def DjangoSerializerException(exc):
    """Django Rest-Framework error serializarion."""
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    # Now add the HTTP status code to the response.
    if isinstance(exc, PumpWoodException):
        return {'type': 'Exception', 'msg': str(exc)}
    if isinstance(exc, PumpWoodDataLoadingException):
        return {'type': 'DataLoadingException', 'msg': str(exc)}
    if isinstance(exc, PumpWoodObjectSavingException):
        return {'type': 'DeserializationException', 'msg': str(exc)}
    if isinstance(exc, PumpWoodUnauthorized):
        return {'type': 'Unauthorized', 'msg': str(exc)}
    if isinstance(exc, PumpWoodForbidden):
        return {'type': 'Forbidden', 'msg': str(exc)}
    if isinstance(exc, PumpWoodObjectDoesNotExist):
        return {'type': 'ObjectDoesNotExist', 'msg': str(exc)}

    return None
